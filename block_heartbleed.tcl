##############################################
# Name: heatbleed.c rejector irule.
# Description: This irule is a tweak to https://devcentral.f5.com/articles/ssl-heartbleed-irule-update
# Purpose: to block heartbleed requests.
# - added check for 768 and 769 ( SSLv3 and TLSv1 )
# - Ensure r is a positive value. This only happens when there is no valid SSL record.
# VERSION: 4 - 16.apr.14
##############################################
when CLIENT_ACCEPTED {
    TCP::collect
    set s 0
    set r 0
}
when CLIENT_DATA {
    set c [TCP::payload length]
    set i 0
    while { $i < $c } {
        set b [expr {$c - $i}]
        if { $s } {
            # skipping payload
            if { $b >= $r } {
                set s 0
                set i [expr {$i + $r}]
            } else {
                set r [expr {$r - $b}]
                set i [expr {$i + $b}]
            }
        } else {
            # parsing TLS record header
            if { $b < 5 } {            
               break
            }
            binary scan [TCP::payload] @${i}cSS t v r
            set r [expr {$r & 0xFFFF}]
            set i [expr {$i + 5}]
            if { $t == 24 }{
                switch -- $v {
                    "768" -
                    "769" -
                    "770" -
                    "771" -
                    "772" {
                         log local0. "Detected Heartbeat Request from [IP::remote_addr]. REJECTING!"
                         reject
                    }
                }
            }
            set s 1
        }
    }
    TCP::release $i
    TCP::collect
}