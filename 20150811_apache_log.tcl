# Function : Apache Logger - log in a standard apache format for the LCE to understand
# Created : 03/08/2015
# Author  : Allan Stones
# History : 03/08/2015 - Initial release
#           11/08/2015 - Added Client IP
#                      - Added HTTP Referer
#                      - Added HTTP URL
#                      - Formatted log to be sd-syslog (Structured Syslog)
#

when HTTP_REQUEST {  
  # Need to breakdown the VS Name as it contains the partition name as well IE: /Common/VS_ecm2.fxdms.net
  set vsshortname [lindex [split [virtual name] "/"] 2]
  set method [HTTP::method]
  set url [HTTP::url]
  set uri [HTTP::uri]
  set version [HTTP::version]
  set referer [HTTP::header "Referer"]
  set clientip [IP::client_addr]
}

when HTTP_RESPONSE {
  set status [HTTP::status]
  set length [HTTP::payload length]
  # This is the Generic SYSLOG message to format to Apache style
  set logMessage "virtual_server=$vsshortname http_refer=$referer client_ip=$clientip http_method=$method http_url=$url http_uri=$uri http_version=$version http_status=$status content_length=$length"

  log 10.101.184.68 local0.info $logMessage
}