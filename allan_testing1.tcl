when RULE_INIT {
	# RULE_INIT is called once for each TMM unit. If you have 8 TMM it will be called 8 times.
	
	# Enable to debug ProxyPass translations via log messages in /var/log/ltm
	# (2 = verbose, 1 = essential, 0 = none)
	set static::Debug 1
	
}

when HTTP_REQUEST {
	set tbl vsratelimit_/Common/VS_ecm2.fxdms.net
	
	switch -glob [string tolower [HTTP::uri]] {
		"/status" { 	
	set rowCount [table keys -subtable $tbl -count]
	
	set response "<html><head><title>Status</title>"
	#append response "<meta http-equiv='refresh' content='10'>"
	append response "<style type='text/css'>\
table.gridtable {\
	font-family: verdana,arial,sans-serif;\
	font-size:11px;\
	color:#333333;\
	border-width: 1px;\
	border-color: #666666;\
	border-collapse: collapse;\
}\
table.gridtable th {\
	border-width: 1px;\
	padding: 8px;\
	border-style: solid;\
	border-color: #666666;\
	background-color: #b8d1f3;\
}\
table.gridtable td {\
	border-width: 1px;\
	padding: 8px;\
	border-style: solid;\
	border-color: #666666;\
	#background-color: #ffffff;\
}\
table.gridtable tr:nth-child(odd) {\
	background-color: #dedede;\
}\
table.gridtable tr:nth-child(even) {\
	background-color: #ededed;\
}\
</style>"
	append response "</head><body><h1>Status</h1><br>"
	
	append response "<h2>Total Row Count=$rowCount</h2><br>"
	append response "<form><input type=\"button\" value=\"Heatmap\" onclick=\"location.href='/heatmap';\"/> | <input type=\"button\" value=\"Refresh\" onclick=\"location.href='/status';\"/> | <input type=\"button\" value=\"Clear Table\" onclick=\"location.href='/clear';\"/></form>"
	append response "<table id='tablesort'class='gridtable'><thead><tr><th>IP Address</th><th>Country</th><th>Lifetime</td><th>Secure Token</th></tr></thead><tbody>"
	
	foreach key [table keys -subtable $tbl] {
		set ipaddress [lindex [split $key ":"] 0]
		set securetoken [lindex [split [lindex [split $key ":"] 1] "="] 1]
		set lifetime [table lifetime -subtable $tbl -remaining $key]
		set country [whereis $ipaddress country]

	    append response "<tr><td><a href='https://gwhois.org/$ipaddress+dns' target='_blank'>$ipaddress</a></td><td>$country</td><td>$lifetime</td><td>$securetoken</td></tr>"
    }	
	
	append response "</tbody></table>"
	append response "</body></html>"
	
	HTTP::respond 200 content $response Content-Type "text/html"			
	}
		"/clear" {
			table delete -subtable $tbl -all
            if { $static::Debug > 0 } {
                log 10.101.64.220 local0.warning "$tbl data has been cleared."
			}			
			HTTP::redirect "http://[HTTP::host]/status"
		} 
		"/heatmap" {
            set chld ""
            set chd ""
            foreach key [table keys -subtable $tbl] {
				set ipaddress [lindex [split $key ":"] 0]
				set country [whereis $ipaddress country]
				if {[table incr -subtable heatmap -mustexist $country] eq ""} {
					table set -subtable heatmap $country 1 indefinite indefinite
			    }
            }
			foreach country [table keys -subtable heatmap] {
			    append chld $country
			    append chd "[table lookup -subtable heatmap $country],"
		    }
			set chd [string trimright $chd ","]
			set response "<html><head><title>Heatmap</title></head><body><center><h1>Here is your sites usage by Country:</h1><br><br><br>"
	        append response "<form><input type=\"button\" value=\"Status Page\" onclick=\"location.href='/status';\"/></form>"			
			append response "<img src='http://chart.googleapis.com/chart?cht=t&amp;chs=440x220&amp;chtm=world&amp;chd=t:$chd&amp;chld=$chld&amp;chco=f5f5f5,edf0d4,6c9642,365e24,13390a&amp;chof=png' border='1' id='chart' alt='Heat Map'/><br></center></body></html>"
            HTTP::respond 200 content $response 	
			table delete -subtable heatmap -all
		}
		default { return }
    }
}