# Generic IES iRule to drop blocked context roots with HTTP404 and log
when HTTP_REQUEST {
  # Declare variables
  set connectMessage "CONNECTED, Source IP: [IP::client_addr], Destination IP: [IP::local_addr], HTTP Method: [HTTP::method], Pool Member: [LB::server addr]:[LB::server port], URL: [HTTP::host], URI: [HTTP::uri]"
  set dropMessage "DROPPED, Source IP: [IP::client_addr], Destination IP: [IP::local_addr], HTTP Method: [HTTP::method], Pool Member: [LB::server addr]:[LB::server port], URL: [HTTP::host], URI: [HTTP::uri]"

  # Check the requested path (set to lowercase)
  # -glob: allow string pattern matching
  switch -glob [string tolower [HTTP::path]] {
    "/" -
	"/status*" -
	"/web-console*" - 
	"/jmx-console*" -
    "*admin_*"	{
	  # All of the above matches DROP TRAFFIC with HTTP404
	  log 10.101.64.220 local0.warning $dropMessage 
	  HTTP::respond 404
	}
	default {
	  # Doesn't match the above so LOG ONLY
	  log 10.101.64.220 local0.info $connectMessage   
	}
  }
  # Cleanup
  unset connectMessage 
  unset dropMessage 
}