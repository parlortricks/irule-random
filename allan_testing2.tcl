when RULE_INIT {
	# RULE_INIT is called once for each TMM unit. If you have 8 TMM it will be called 8 times.
	
	# Enable to debug ProxyPass translations via log messages in /var/log/ltm
	# (2 = verbose, 1 = essential, 0 = none)
	set static::Debug 1
	
}

when HTTP_REQUEST {
	set tbl vsratelimit_/Common/VS_ecm2.fxdms.net
	
	switch -glob [string tolower [HTTP::uri]] {
		"/clear" {
			table delete -subtable $tbl -all
            if { $static::Debug > 0 } {
                log 10.101.64.220 local0.warning "$tbl data has been cleared."
			}			
			HTTP::redirect "http://[HTTP::host]/status"
		} 
		"/status" {
			set rowCount [table keys -subtable $tbl -count]
			set response "<html><head><title>Status</title></head><body>\
<h2>Total Row Count=$rowCount</h2><br>\
<form><input type=\"button\" value=\"Heatmap\" onclick=\"location.href='/heatmap';\"/> | <input type=\"button\" value=\"Refresh\" onclick=\"location.href='/status';\"/> | <input type=\"button\" value=\"Clear Table\" onclick=\"location.href='/clear';\"/></form>\
<script type='text/javascript' src='https://www.google.com/jsapi'></script>\
<div id='stringFilter_dashboard_div' style='border: 1px solid #ccc'>\
<div id='stringFilter_control_div' style='padding-left: 2em'></div>\
<div id='stringFilter_chart_div'></div></div><script type='text/javascript'>\
google.load('visualization', '1.0', {'packages':\['corechart', 'table', 'gauge', 'controls'\]});\
google.setOnLoadCallback(drawStringFilter);\
function drawStringFilter() {\
var dashboard = new google.visualization.Dashboard(document.getElementById('stringFilter_dashboard_div'));\
var control = new google.visualization.ControlWrapper({'controlType': 'StringFilter','containerId': 'stringFilter_control_div','options': {'filterColumnIndex': 0}});\
var chart = new google.visualization.ChartWrapper({'chartType': 'Table','containerId': 'stringFilter_chart_div', 'options': {'allowHtml': 'True'}});\
var data = google.visualization.arrayToDataTable(\[\
\['IP Address', 'Country', 'Lifetime', 'Secure Token'\],"

	        foreach key [table keys -subtable $tbl] {
	
		        set ipaddress [lindex [split $key ":"] 0]
		        set securetoken [lindex [split [lindex [split $key ":"] 1] "="] 1]
		        set lifetime [table lifetime -subtable $tbl -remaining $key]
		        set country [whereis $ipaddress country]

	            append response "\['<a href=\"https://gwhois.org/$ipaddress+dns\" target=\"_blank\">$ipaddress</a>','$country','$lifetime','$securetoken'\],"
            }
			
			append response "\]);dashboard.bind(control, chart);dashboard.draw(data);}</script></body></html>"
			HTTP::respond 200 content $response
	    }
		"/heatmap" {
			set response "<html><head>\
<script type='text/javascript' src='https://www.google.com/jsapi'></script>\
<script type='text/javascript'>\
google.load('visualization', '1', {packages:\['geochart'\]});\
google.setOnLoadCallback(drawRegionsMap);\
function drawRegionsMap() {\
var data = google.visualization.arrayToDataTable(\[\
\['Country', 'Blocked'\],"

            foreach key [table keys -subtable $tbl] {
				set ipaddress [lindex [split $key ":"] 0]
				set country [whereis $ipaddress country]
				if {[table incr -subtable heatmap -mustexist $country] eq ""} {
					table set -subtable heatmap $country 1 indefinite indefinite
			    }
            }
			
			foreach country [table keys -subtable heatmap] {
				append response "\['$country',[table lookup -subtable heatmap $country]\],"
		    }
			
            append response "\]);var options = {};var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));\
chart.draw(data, options);}\
</script></head><body>\
<form><input type='button' value='Status Page' onclick=\"location.href='/status';\"/> | <input type='button' value='Refresh' onclick=\"location.href='/heatmap';\"/></form>\
<center><div id='regions_div' style='width: 900px; height: 500px;'></div></center></body></html>"

            HTTP::respond 200 content $response
			table delete -subtable heatmap -all
	    }
		default { return }
    }
}