when HTTP_REQUEST {
     if { [HTTP::host] equals "ecm2.fxdms.net" and [HTTP::uri] equals "/"} {
     drop
	 log 10.101.64.220 local0.warning "Client Dropped, Source IP: [IP::client_addr], Destination IP: [IP::local_addr], HTTP Method: [HTTP::method], Pool Member: [LB::server addr]:[LB::server port], URL: [HTTP::host], URI: [HTTP::uri]"
}
     if { [HTTP::host] equals "ecm2.fxdms.net" and [HTTP::uri] starts_with "/status"} {
     drop
	 log 10.101.64.220 local0.warning "Client Dropped, Source IP: [IP::client_addr], Destination IP: [IP::local_addr], HTTP Method: [HTTP::method], Pool Member: [LB::server addr]:[LB::server port], URL: [HTTP::host], URI: [HTTP::uri]"
}
     if { [HTTP::host] equals "ecm2.fxdms.net" and [HTTP::uri] equals "/web-console/"} {
     drop
	 log 10.101.64.220 local0.warning "Client Dropped, Source IP: [IP::client_addr], Destination IP: [IP::local_addr], HTTP Method: [HTTP::method], Pool Member: [LB::server addr]:[LB::server port], URL: [HTTP::host], URI: [HTTP::uri]"
}
     if { [HTTP::host] equals "ecm2.fxdms.net" and [HTTP::uri] equals "/jmx-console/"} {
     drop
	 log 10.101.64.220 local0.warning "Client Dropped, Source IP: [IP::client_addr], Destination IP: [IP::local_addr], HTTP Method: [HTTP::method], Pool Member: [LB::server addr]:[LB::server port], URL: [HTTP::host], URI: [HTTP::uri]"
}
    set list_of_path [split [URI::decode [HTTP::uri]] "/"]
    set data_group_name dg_urisec_[lindex $list_of_path 1]

    # If a data group exists with the data group name then apply restriction
    # If the data group doesn't exist there is no IP restriction and continue

    if {[class exists $data_group_name]} {
        if {[class search $data_group_name equals [IP::client_addr]] == 0} {
            HTTP::respond 404
        }
    }
}