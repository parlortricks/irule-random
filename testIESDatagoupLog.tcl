when HTTP_REQUEST {  
  if {[matchclass [string tolower [HTTP::path]] equals $::dg_ies_blocked_paths]}{ 
    log 10.101.64.220 local0.warning "DROPPED HTTP::path"
  } else {
    log 10.101.64.220 local0.warning "CONNECTED HTTP::path"
  }
}
