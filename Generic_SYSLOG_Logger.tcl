when HTTP_REQUEST {
  # This is the Generic SYSLOG message
  set logMessage "Client Connected, Source IP: [IP::client_addr], Destination IP: [IP::local_addr], HTTP Method: [HTTP::method], Pool Member: [LB::server addr]:[LB::server port], URL: [HTTP::host], URI: [HTTP::uri]"
  
  # Check the requested path (set to lowercase)
  # -glob: allow string pattern matching
  switch -glob [string tolower [HTTP::path]] {
    "/" -
	"/status*" -
	"/web-console*" - 
	"/jmx-console*" -
    "*admin_*"	{
	  # All of the above matches use WARNING
	  log 10.101.64.220 local0.warning $logMessage
	}
	default {
	  # Doesn't match the above so use INFO
      log 10.101.64.220 local0.info $logMessage
	}
  }
  # Cleanup
  unset logMessage
}