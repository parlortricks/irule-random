# Function : Standard Logger - log in a structured syslog (sd-syslog) format for the LCE to understand
# Author  : Allan Stones

when CLIENT_ACCEPTED {
	# Make sure you change the pool below to the logger you want to send to
	set logServer [HSL::open -proto UDP -pool pool_tennable_hk]
}

when HTTP_REQUEST {  
	# Need to breakdown the VS Name as it contains the partition name as well IE: /Common/VS_something.com
	set vsshortname [lindex [split [virtual name] "/"] 2]
	set method [HTTP::method]
	set url [HTTP::host]
	set uri [HTTP::uri]
	set version [HTTP::version]
	set referer [HTTP::header "Referer"]
	set clientip [IP::client_addr]
	set http_request_time [clock clicks -milliseconds]  
}

when LB_SELECTED {
	# Record the pool name, server ip and server port
	set lb_server_pool [lindex [split [LB::server pool] "/"] 2]
	set lb_server_addr [LB::server addr]
	set lb_server_port [LB::server port]
}

when HTTP_RESPONSE {
	set date_time [clock format [clock seconds] -format {%Y/%m/%d %H:%M:%S}]
	set status [HTTP::status]
	set length [HTTP::payload length]
	set http_response_time [expr {[clock clicks -milliseconds] - $http_request_time}]

	# Join all the strings together with a comman "," as the delimiter
	set logMessage "$date_time,$vsshortname,$referer,$clientip,$method,$url,$uri,$version,$status,$length,$lb_server_pool,$lb_server_addr,$lb_server_port,$http_response_time"

	HSL::send $logServer $logMessage
}
