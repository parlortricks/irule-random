# Function : Apache Logger - log in a standard apache format for the LCE to understand
# Created : 03/08/2015
# Author  : Allan Stones
# History : 03/08/2015 - Initial release
#
# EXAMPLE LOGS
#10.120.220.167 - - [03/Aug/2015:10:00:32 +1000] "GET /" 403 210
#203.0.138.17 - merc_prd [03/Aug/2015:10:00:33 +1000] "POST /merc_prd/ies/ivlogin.iv_login?user=Mercer&pwd=12345 HTTP/1.1" 200 24
#203.0.138.17 - merc_prd [03/Aug/2015:10:00:33 +1000] "GET /merc_prd/ies/ivpget.iv_pget?SessionKey=4qMQUERFiW0003094D04&externalid=MERCERB.00927436.0001.4 HTTP/1.1" 200 699709
#203.0.138.17 - merc_prd [03/Aug/2015:10:00:33 +1000] "GET /merc_prd/ies/ivlogin.iv_login?user=Mercer&pwd=12345 HTTP/1.1" 200 24
#203.0.138.17 - merc_prd [03/Aug/2015:10:00:33 +1000] "GET /merc_prd/ies/ivpget.iv_pget?SessionKey=KStzSgd02a0003094D05&externalid=MERCERB.00831663.0001.4 HTTP/1.1" 200 542313

when HTTP_REQUEST {  
  # Need to breakdown the VS Name as it contains the partition name as well IE: /Common/VS_ecm2.fxdms.net
  set vsshortname [lindex [split [virtual name] "/"] 2]
  set method [HTTP::method]
  set uri [HTTP::uri]
  set version [HTTP::version]
}

when HTTP_RESPONSE {
  set status [HTTP::status]
  set length [HTTP::payload length]
  # This is the Generic SYSLOG message to format to Apache style
  set logMessage "$vsshortname \"$method $uri $version\" $status $length"

  log 10.101.184.68 local0.info $logMessage
}