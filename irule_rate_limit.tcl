# Function : RateLimit HTTP GET requests per IP, per URI
# Created : 09/02/2015
# Author  : Allan Stones
# History : 10/02/2015 - Fixed up the Data Group checking
#                      - Added checks to see if data groups exist
#                      - Fixed up VS name stuff
#                      - Added debug check for logging
#                      - Log the URI we match on
#                      - Log the country the IP is from based on F5 whereis database
#                      - Log table row count each time we drop connection to show table size at that time
 
when RULE_INIT {
	# RULE_INIT is called once for each TMM unit. If you have 8 TMM it will be called 8 times.
	
	# Enable to debug ProxyPass translations via log messages in /var/log/ltm
	# (2 = verbose, 1 = essential, 0 = none)
	set static::Debug 1
	
    # What is the timeout of these connections in seconds before they can have another
    set static::timeoutSecs 60 

    # Generate a subtable name unique to this iRule and virtual server
    #   We use the virtual server name with this prefix (vsratelimit_<vs name>)
    set static::tbl "vsratelimit"
}
 
when HTTP_REQUEST {
    # Need to breakdown the VS Name as it contains the partition name as well IE: /Common/VS_ecm2.fxdms.net
    set vsshortname [lindex [split [virtual name] "/"] 2]
	
	# Which country is the client coming from?
	set country [whereis [IP::client_addr] country]

    # Check if dg_ratelimit_uri_$vsshortname exists. No point processing any further if it doesnt
	if { [class exists "dg_ratelimit_uri_$vsshortname"] } {
	
	
        if { ([HTTP::method] eq "GET") and ([class match [string tolower [HTTP::uri]] starts_with "dg_ratelimit_uri_$vsshortname"] ) } {
		    set uriMatch [class match -name [string tolower [HTTP::uri]] starts_with "dg_ratelimit_uri_$vsshortname"]
            # Allow a whitelist of IP addresses to not be rate limited
			# Check to see if dg_ratelimit_whitelist_$vsshortname exists
			if { [class exists "dg_ratelimit_whitelist_$vsshortname"] } {
                if { [class match [IP::client_addr] equals "dg_ratelimit_whitelist_$vsshortname"] }{
                   return
                }
		    }
 
            # set variables
            set key "[IP::client_addr]:[HTTP::uri]"
        
            # Save the virtual server-specific subtable name (vsratelimit_<vs name>)
            set tbl ${static::tbl}_[virtual name]        
        
        
            if { [table lookup -subtable $tbl $key] != "" } {
				set rowCount [table keys -subtable $tbl -count]
                if { $static::Debug > 0 } {
                    log 10.101.64.220 local0.warning "Country=$country, URI=$uriMatch, Rows=$rowCount, $key has exceeded the number of requests allowed."
			    }
				# Drop traffic. Should be changed to a HTTP response code based on how the application works.
			    # This is a very abrupt way to deal with this
                #drop
                return
            } else {
			    # Add IP:URI pair to the table with a timeout of $static::timeoutSecs
                table add -subtable $tbl $key " " indefinite $static::timeoutSecs
            }
        }
    }
}